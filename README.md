# airwallex-devops-challenge

This repo is built for the airwallex devops challenge

## Experiment Environment

**Below are the version of tools should be used:**

- ansible version: ansible 5 [core 2.12.2]
- python version: 3.9.4

## Before you start

**Before Starting using these playbooks, make sure all the things below are checked:**

1. Make sure python (3.6 or greater) is installed
2. Make sure ansible (>=2.9.10) is installed, since it is required by the ansible module `aws` https://galaxy.ansible.com/amazon/aws
3. We are going to provision aws ec2 services, because its free tier make it much easier for us to test the codes.
4. Please create the access key first, then you should  set them as the local environment variables; or you can just put them into `group_vars/all`
5. Make sure necessary packages are installed 
   ```
   pip3 install boto boto3 beautifulsoup4 pyyaml re 
   ```
6. `Ubuntu 18.04 Bionic` will be used for testing the auto-deployment of nginx.
7. Below will be the structure of working directory all things are done:
```
.
├── README.md
├── ansible.cfg
├── ansible_facts
│   ├── ec2-3-90-29-111.compute-1.amazonaws.com
│   └── localhost
├── aws.newcreated.keypair.pem
├── group_vars
│   └── all
├── inventory.yaml
├── inventory.yaml.j2
├── logs
│   └── ansible.log
├── playbooks
│   ├── all.yaml
│   ├── bootstrap.yaml
│   ├── create-instance.yaml
│   ├── create-vpc.yaml
│   ├── delete-vpc.yaml
│   ├── deploy-cadvisor.yaml
│   ├── deploy-nginx.yaml
│   ├── install-docker.yaml
│   ├── roles
│   │   ├── cadvisor
│   │   │   ├── tasks
│   │   │   │   └── main.yaml
│   │   │   └── templates
│   │   │       └── start_cadvisor.sh.j2
│   │   ├── docker
│   │   │   ├── defaults
│   │   │   │   └── main.yaml
│   │   │   ├── handlers
│   │   │   │   └── main.yaml
│   │   │   ├── tasks
│   │   │   │   └── main.yaml
│   │   │   └── templates
│   │   │       └── daemon.json.j2
│   │   └── nginx
│   │       ├── tasks
│   │       │   └── main.yaml
│   │       └── templates
│   │           ├── Dockerfile.j2
│   │           ├── default.conf.j2
│   │           ├── healthz.txt.j2
│   │           ├── index.html.j2
│   │           └── start_nginx.sh.j2
│   └── terminate-instance.yaml
└── tools
    └── fetch.py
```
8. We are using the `ansible.cfg` config file for all the ansible commands or playbooks, so make sure all `ansible` commands are executed in root of this directory.
9. The new vpc, Internet gateway, subnet, ACL, security group and route table will be created.
   1. - For security group, will only expose inbound port for `ssh`, `nginx` server and `cadvisor`. No limit for outbound.
   2. - For ACL ingress, icmp are limited to avoid `ping`. As for egress, since we are going to use public docker hub, no block is set either.
10. When launching the instance, an extra data device must be create. This disk will be mainly used by the docker data. This aws volume can be automatically deleted when the instance is terminated.
11. When launching the instance, you can use the existed keypair, or it will create a new one.


## Configuration

Before you start to actually setup the resources, we need to configure the `group_vars/all` first, which will, to some extend, decide how the infrastructures finally look like.

## Get Started
### Create Amazon EC2 VPC
This step will complete the VPC creation and configuration.
```
ansible-playbook playbooks/create-vpc.yaml
```

### Create Amazon EC2 Instance
This step will use a specific keypair( or new created one)  to launch instance. After this step, 

- If you use a new keypair, a private key pem file will be generated in the root of working directory, and it will be used for later ansible commands. 
- The ansible inventory file `inventory.yaml` will be generated, and we will use the `launched` group as our later ansible hosts.
```
ansible-playbook playbooks/create-vpc.yaml
```

### Bootstrap the instance
In this step, we will mount the data device and stop firewalld.
```
ansible-playbook playbooks/bootstrap.yaml  --private-key=aws.newcreated.keypair.pem --user=ubuntu
```

### Install Docker
`daemon.json` can be configured.
```
ansible-playbook playbooks/install-docker.yaml  --private-key=aws.newcreated.keypair.pem --user=ubuntu
# or
ansible-playbook playbooks/install-docker.yaml  --private-key=path/to/your/private-key.pem --user=ubuntu
```

### Deploy Nginx
In this step, we are going to deploy nginx. After this step, we can visit it default page from your local browser.
The address shoule be the public dns url of the instance, like `http://ec2-3-90-29-111.compute-1.amazonaws.com`.

The default page (index.html.j2) html can be modified to test our python tool.

```
ansible-playbook playbooks/deploy-nginx.yaml  --private-key=aws.newcreated.keypair.pem --user=ubuntu
# or
ansible-playbook playbooks/deploy-nginx.yaml  --private-key=path/to/your/private-key.pem --user=ubuntu
```

### Fetch the text from Nginx default page

This will parse the inventory yaml, and it also finds the most frequent word in this page.

```
python3 tools/fetch.py
```

### Deploy cAdvisor for monitoring

This will parse the inventory yaml, and it also finds the most frequent word in this page.

You visit cadvisor through address like `http://ec2-3-90-29-111.compute-1.amazonaws.com:9999`.
```
ansible-playbook playbooks/deploy-cadvisor.yaml  --private-key=aws.newcreated.keypair.pem --user=ubuntu
# or
ansible-playbook playbooks/deploy-cadvisor.yaml  --private-key=path/to/your/private-key.pem --user=ubuntu
```

## Appendix 1： Container Health Check
- The new docker image for nginx will be built, it includes a simple healthcheck.
- A healthcheck file is added into nginx root path, we can use monitoring tool, like `Kibana Uptime`, to check its healthness.

## Appendix 2： Monitoring 
- You can check the resource usage through cAdvisor home page, it has a fansy UI.
- Or you can use prometheus to scrap its metrics.
- Or you can use a script to parse the data from http://ec2-3-90-29-111.compute-1.amazonaws.com:9999/metrics and log them, every 10s.

## Delete the instances and network
```
ansible-playbook playbooks/terminate-instance.yaml
# and
ansible-playbook playbooks/delete-vpc.yaml
```