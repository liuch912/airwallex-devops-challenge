from cgitb import text
from json import load
from urllib.request import urlopen
from bs4 import BeautifulSoup
from collections import Counter
import yaml
import re

def main():
    url = 'http://' + load_host()
    text = request_trim(url)

    # Output the trimed html text
    print(text)

    word = find_most_common(text)

    print("\nThe most common word count:")
    print(word)

def load_host():
    # read the first of ansible inventory hosts inventory.yaml
    with open("inventory.yaml", "r") as stream:
        host_dict = yaml.safe_load(stream)
    # return host_dict['all']['children']['launched']['hosts'][0]
    return list(host_dict.get('all').get('children').get('launched').get('hosts').keys())[0]


def request_trim(url):
    html = urlopen(url).read()
    soup = BeautifulSoup(html, features="html.parser")

    # remove all script and style elements
    for script in soup(["script", "style"]):
        script.extract()    # rip it out

    # get text
    text = soup.get_text()

    # break into lines and remove leading and trailing space on each
    lines = (line.strip() for line in text.splitlines())
    
    # break multi-headlines into a line each
    chunks = (phrase.strip() for line in lines for phrase in line.split("  "))
    
    # drop blank lines and add a space at the end of each line
    text = '\n'.join(chunk + " " for chunk in chunks if chunk)

    return text

def find_most_common(str):

    # remove special characters
    alphanumeric_str = re.sub(r'[^A-Za-z0-9 ]+', '', str)

    # get the list of all the words in the string
    split_it = alphanumeric_str.split()

    # pass the word list to instance of Counter class.
    Counters_found = Counter(split_it)

    # most_common() produces k frequently encountered, in this case k = 1
    # input values and their respective counts.
    most_occur = Counters_found.most_common(1)
    
    return(most_occur)

main()